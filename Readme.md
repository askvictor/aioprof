# aioprof

Micropython tool for basic profiling of asyncio applications.

Run `micropython example_aioprof.py` to demonstrate basic usage.

```
mpremote mip install https://gitlab.com/alelec/aioprof/-/raw/main/aioprof.py
mpremote mip install aiorepl
mpremote mount . repl
```

## usage
aioprof can be added into you code, eg 
```
import aioprof
aioprof.enable()
```
after which all new asyncio tasks created will be captured and measured. The outputs of
which can be viewed with `aioprof.report()` or `aioprof.json()`

It can also be enabled at runtime / on aiorepl with:
```
import aioprof
aioprof.inject()
```
which will attempt to hook into all existing tasks as well as new ones.

## example

```
import time
import uasyncio as asyncio
import aioprof

aioprof.enable()


async def quicker():
    while True:
        # This is being used as a standin for any blocking operation,
        # like a filesystem write, spi transfer, i2c scan etc.
        time.sleep_ms(1)

        await asyncio.sleep_ms(10)


async def slow():
    while True:
        # A much longer blocking operation
        time.sleep_ms(80)

        await asyncio.sleep_ms(10)


async def main():
    asyncio.create_task(slow())
    asyncio.create_task(quicker())

    await asyncio.sleep_ms(500)

    # print(aioprof.json())
    aioprof.report()


asyncio.run(main())
```

Which looks like this when run:
```
# micropython example_aioprof.py
┌──────────────────────────────────────────────┬───────┬─────┐
│ function name                                │ count │ ms  │
├──────────────────────────────────────────────┼───────┼─────┤
│ <generator object 'slow' at 7f8308677120>    │ 6     │ 480 │
│ <generator object 'quicker' at 7f8308677440> │ 6     │ 6   │
│ <generator object 'main' at 7f8308676b40>    │ 1     │ 0   │
└──────────────────────────────────────────────┴───────┴─────┘
```

